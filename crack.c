#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int check = 0;
    // Compare the two hashes
    if (strcmp(guess, hash) == 0)
    {
        check = 1;
    }
    return check;
    
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");

    int size =50;
    char **pwds=(char**)malloc(size*sizeof(char*));
    char str[size];
    int i=0;
    //scan file into string array
    while(fscanf(f, "%s",str) != EOF)
    {
        //increase size of the pwds array if neccessary
        if (i==size)
        {
            size+= 10;
            char**newarr=(char**)realloc(pwds, size*sizeof(char*));
            if(newarr!=NULL)
            {
                pwds=newarr;
            }
            else
            {
                printf("Realloc fails\n");
                exit(1);
            }
        }
        char *newstr=(char*)malloc((strlen(str)+1)*sizeof(char));
        //copy the read in string to a new string
        strcpy(newstr,str);
        //put the new string into the pwds array
        pwds[i]=newstr;
        i++;

    }
    //increase the size for the nulled ending if necessary
    if (i == size)
    {
    size = size + 1;
    char **newarr = (char **)realloc(pwds, size * sizeof(char *));
    if (newarr != NULL) pwds = newarr;
    else exit(1);
    }
    //close file
    fclose(f);
    return pwds;
}


//Make a 3rd array that stores the hashed passwords(use tryguess to compare strings in main())
char **newdict(char **dict)
{
    int size = 50;
    char **newdict = (char**)malloc(size * sizeof(char *));
    int i = 0;
    while (dict[i] != NULL) 
    {   
        //increase size of the newDict array if neccessary
        if (i == size)
        {
            size = size + 50;
            char **newarr = (char **)realloc(newdict, size * sizeof(char *));
            if (newarr != NULL) newdict = newarr;
            else exit(1);
        }
        //Hash with md5
        char *dictHash = md5(dict[i], strlen(dict[i]));
        newdict[i] = dictHash;
        i++;
    }
    //increase size for nulled ending
    if (i == size)
    {
        size = size + 1;
        char **newarr = (char **)realloc(newdict, size * sizeof(char *));
        if (newarr != NULL) newdict = newarr;
        else exit(1);
    }
    newdict[i] = NULL;
    
    return newdict;
}
int main(int argc, char *argv[])
{
    
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    char **hdict = newdict(dict);
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int hCount = 0;
    int dCount = 0;
    while (hashes[hCount] != NULL)
    {
        while (hdict[dCount] != NULL)
        {
            if (tryguess(hashes[hCount], hdict[dCount]) == 1)
            {
                printf("%s %s\n", hashes[hCount], dict[dCount]);
                break;
            }
            dCount++;
        }
        dCount = 0;
        hCount++;
    }
    //Free memory
    for (int i = 0; i < hCount; i++)
    {
        free(hashes[i]);
    }
    
    int count = 0;
    while (dict[count] != NULL)
    {
        free(dict[count]);
        free(hdict[count]);
        count++;
    }
    
    free(hashes);
    free(dict);
    free(hdict);
}
